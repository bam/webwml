#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans le serveur de base de données
MySQL. Les vulnérabilités sont corrigées en mettant à niveau MySQL vers la
nouvelle version 5.5.62 de l’amont, qui inclut des modifications
supplémentaires. Veuillez consulter les notes de publication de MySQL 5.5 et les
annonces de correctif critique pour mise à niveau d’Oracle pour de plus amples
détails :</p>

<ul>
 <li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-61.html"></li>
 <li><a href="https://www.oracle.com/technetwork/security-advisory/cpujul2018-4258247.html">https://www.oracle.com/technetwork/security-advisory/cpujul2018-4258247.html</a></li>
 <li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-62.html">https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-62.html</a></li>
 <li><a href="https://www.oracle.com/technetwork/security-advisory/cpuoct2018-4428296.html">https://www.oracle.com/technetwork/security-advisory/cpuoct2018-4428296.html</a></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 5.5.62-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1566.data"
# $Id: $
