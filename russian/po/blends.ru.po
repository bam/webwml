msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-01-07 14:42+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Метапакеты"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Загрузки"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Производные дистрибутивы"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Целью Debian Astro является разработка операционной системы на основе "
"Debian, которая подходила бы под требования и профессиональных астрономов, и "
"астрономов-любителей. Эта смесь включает в себя большое число пакетов с "
"программным обеспечением для управления телескопами, обработки данных, "
"презентаций и многого другого."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"Целью DebiChem является превращение Debian в хорошую платформу для химиков, "
"которая подходила бы им в их ежедневной работы."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Целью Debian Games является предоставление игр в Debian, начиная от аркад и "
"приключений и заканчивая симуляторами и стратегиями."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Целью Debian Edu является превращение операционной системы Debian в систему, "
"подходящую для образовательных целей и использования в школах."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"Целью Debian GIS является превращение Debian в лучший дистрибутив для работы "
"с приложениями, связанными с геоинформационными системами."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Целью Debian Junior является превращение Debian в операционную систему, "
"которую с радостью бы использовали дети."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Целью Debian Med является создание полностью свободной и открытой системы "
"для выполнения всех задач, возникающий при оказании медицинских услуг и при "
"проведении медицинских исследований. Для достижения этой цели проект Debian "
"Med занимается интеграцией релевантного свободного ПО и ПО с открытым "
"исходным кодом для медицинского сканирования, биоинформатики, клинической ИТ-"
"инфраструктуры и проч. в операционную систему Debian."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Целью Debian Multimedia является превращение Debian в хорошую платформу для "
"работы с аудио и мультимедиа."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Целью Debian Science является упрощение использования Debian исследователями "
"и учёными."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Целью Debian Accessibility является превращение Debian в операционную "
"систему, которая бы подходила под требования людей с ограниченными "
"возможностями."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Целью Debian Design является предоставление приложений для дизайнеров. Сюда "
"включается графический дизайн, веб-дизайн и дизайн мультимедиа."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"Целью Debian EzGo является предоставление открытых и свободных технологий с "
"поддержкой родных языков, а также удобного для пользователя, легковесного и "
"быстрого окружения рабочего стола для маломощного/дешёвого оборудования с "
"целью увеличения возможностей людей и стимуляции развития технологий в таких "
"странах и регионах как Африка, Афганистан, Индонезия, Вьетнам."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"Целью FreedomBox является разработка и популяризация использования "
"персональных серверов, работающих под управлением свободного ПО для "
"обеспечения частных личных коммуникаций. Данная смесь включает в себя ПО для "
"обеспечения работы блогов, вики, веб-сайтов, социальных сетей, электронной "
"почты, веб-прокси и ретранслятора Tor на устройстве, которое может заменить "
"собой беспроводной маршрутизатор так, чтобы все данные оставались под "
"властью пользователей."

#: ../../english/blends/unreleased.data:46
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Целью Debian Hamradio является поддержка нужд радиолюбителей в Debian путём "
"предоставления приложений для журналирования, режима данных и пакетного "
"режима, а также других целей."

#: ../../english/blends/unreleased.data:55
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"Целью DebianParl является предоставление приложений для поддержки нужд "
"парламентариев, политиков и их помощников по всему миру."

#~ msgid "Debian Accessibility"
#~ msgstr "Debian Accessibility"

#~ msgid "Debian Games"
#~ msgstr "Debian Games"
