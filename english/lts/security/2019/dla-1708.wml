<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in Zabbix, a
server/client network monitoring solution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10742">CVE-2016-10742</a>

    <p>Zabbix allowed remote attackers to redirect to external links by
    misusing the request parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2826">CVE-2017-2826</a>

    <p>An information disclosure vulnerability exists in the iConfig proxy
    request of Zabbix server. A specially crafted iConfig proxy request
    can cause the Zabbix server to send the configuration information of
    any Zabbix proxy, resulting in information disclosure. An attacker
    can make requests from an active Zabbix proxy to trigger this
    vulnerability.</p>

<p>This update also includes several other bug fixes and improvements. For
more information please refer to the upstream changelog file.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.2.23+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1708.data"
# $Id: $
