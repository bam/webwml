<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17000">CVE-2018-17000</a>

    <p>A NULL pointer dereference in the function _TIFFmemcmp at tif_unix.c
    (called from TIFFWriteDirectoryTagTransferfunction) allows an
    attacker to cause a denial-of-service through a crafted tiff file. This
    vulnerability can be triggered by the executable tiffcp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19210">CVE-2018-19210</a>

    <p>There is a NULL pointer dereference in the TIFFWriteDirectorySec function
    in tif_dirwrite.c that will lead to a denial of service attack, as
    demonstrated by tiffset.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7663">CVE-2019-7663</a>

    <p>An Invalid Address dereference was discovered in
    TIFFWriteDirectoryTagTransferfunction in libtiff/tif_dirwrite.c,
    affecting the cpSeparateBufToContigBuf function in tiffcp.c. Remote
    attackers could leverage this vulnerability to cause a denial-of-service
    via a crafted tiff file.</p>

    <p>We believe this is the same as <a href="https://security-tracker.debian.org/tracker/CVE-2018-17000">CVE-2018-17000</a> (above).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.0.3-12.3+deb8u8.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1680.data"
# $Id: $
