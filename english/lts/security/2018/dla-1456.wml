<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various vulnerabilities were discovered in graphicsmagick, a collection
of image processing tools and associated libraries, resulting in denial
of service, information disclosure, and a variety of buffer overflows
and overreads.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.20-3+deb8u4.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1456.data"
# $Id: $
