<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>joernchen of Phenoelit discovered that git, a fast, scalable,
distributed revision control system, is prone to an arbitrary code
execution vulnerability via a specially crafted .gitmodules file in a
project cloned with --recurse-submodules.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1.4-2.1+deb8u7.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1533.data"
# $Id: $
