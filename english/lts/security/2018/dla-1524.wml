<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14404">CVE-2018-14404</a>
      <p>Fix of a NULL pointer dereference which might result in a crash and
      thus in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14567">CVE-2018-14567</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-9251">CVE-2018-9251</a>
      <p>Approvement in LZMA error handling which prevents an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18258">CVE-2017-18258</a>

      <p>Limit available memory to 100MB to avoid exhaustive memory
      consumption by malicious files.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.9.1+dfsg1-5+deb8u7.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1524.data"
# $Id: $
