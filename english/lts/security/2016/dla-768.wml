<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The read_binary function in buffer.c in pgpdump, a PGP packet
visualizer, allows context-dependent attackers to cause a denial of
service (infinite loop and CPU consumption) via crafted input. This was
assigned <a href="https://security-tracker.debian.org/tracker/CVE-2016-4021">CVE-2016-4021</a>.</p>

<p>Also, the read_radix64 function the might read data from beyond the
end of a buffer from crafted input.</p>


<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.27-1+deb7u1.</p>

<p>For Debian 8 <q>Jessie</q>, these problems will be fixed in version
0.28-1+deb8u1, part of the upcoming point release</p>

<p>For Debian 9 <q>Stretch</q> and <q>Sid</q>, these problems have been fixed in
version 0.31-0.1</p>

<p>We recommend that you upgrade your pgpdump packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-768.data"
# $Id: $
