<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This is an update of DLA-558-1. The previous build had
revision number that was considered lower than the one
in wheezy and was therefore not installed at upgrade.</p>

<p>The text for DLA-558-1 is included here for reference
(with some improvement).</p>

<p>Two security related problems have been found in the mongodb
package, both related to logging.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6494">CVE-2016-6494</a>

  <p>World-readable .dbshell history file</p></li>

<li>Debian Bug 833087

  <p>Bruteforcable challenge responses in unprotected logfile</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.6-1.1+deb7u1.</p>

<p>We recommend that you upgrade your mongodb packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-588-2.data"
# $Id: $
