<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that apng2gif was vulnerable to an integer overflow
resulting in a heap-based buffer over-read/write. A remote attacker
could use this flaw to cause a denial of service (application crash)
via a crafted APNG file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.5-1+deb7u1.</p>

<p>We recommend that you upgrade your apng2gif packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-981.data"
# $Id: $
