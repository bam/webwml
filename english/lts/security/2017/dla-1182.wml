<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A security vulnerability has been discovered in ldns, a library and
collection of utilities for DNS programming.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000231">CVE-2017-1000231</a>

    <p>The generic parser contained a double-free vulnerability which
    resulted in an application crash with unspecified impacts and attack
    vectors.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.6.13-1+deb7u2.</p>

<p>We recommend that you upgrade your ldns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1182.data"
# $Id: $
