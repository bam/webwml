<define-tag pagetitle>Debian 9 aktualisiert: 9.8 veröffentlicht</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="fdc485a3cb59ae0b5ef29c7ff90f411b2444ed09" maintainer="Erik Pfannenstein"

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die achte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction arc "Verzeichnisüberschreitungen [CVE-2015-9275], arcdie-Abstürze bei Aufrufen mit mehr als 1 Variablen-Argument und Probleme beim Lesen von Version-1-Arc-Headern behoben">
<correction astroml-addons "Python-3-Abhängigkeiten korrigiert">
<correction base-files "Aktualisierung auf Zwischenveröffentlichung">
<correction c3p0 "Anfälligkeit für Referenzierungen von externen XML-Objekten (XML External Entity) behoben [CVE-2018-20433]">
<correction ca-certificates-java "Temporäre jvm-*.cfg-Generierung auf armhf korrigiert">
<correction chkrootkit "Regulären Ausdruck zum Aussieben von dhcpd und dhclient als Fehlalarme beim Paketsniffertest behoben">
<correction compactheader "Aktualisiert, um unter neueren Thunderbird-Versionen zu funktionieren">
<correction courier "@piddir@-Ersetzung behoben">
<correction cups "Sicherheitskorrekturen [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config "Konfiguration persönlicher Webauftritte behoben; Offline-Installation eines Kombi-Servers einschließlich Unterstützung für plattenlose Arbeitsplatzrechner wiederhergestellt; Setzen der Chromium-Startseite zur Installationszeit und via LDAP ermöglicht">
<correction debian-installer "Neubau für die Zwischenveröffentlichung">
<correction debian-installer-netboot-images "Neubau gegen vorgeschlagene Aktualisierungen">
<correction debian-security-support "Neuen Unterstützungsstatus für verschiedene Pakete hinterlegt">
<correction dnspython "Fehler beim Auswerten von nsec3-Bitmaps in Text behoben">
<correction egg "emacsen-install für das nicht unterstützte xemacs21 überspringen">
<correction erlang "Keinen Erlang-Modus für XEmacs installieren">
<correction espeakup "debian/espeakup.service: Kompatibilität mit älteren Versionen von systemd wiederhergestellt">
<correction freerdp "Sicherheitsprobleme behoben [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788]; Unterstützung für CredSSP v3 und RDP-Protokoll v6 hinzugefügt">
<correction ganeti-os-noop "Ermittlung der Größe von Nicht-Blockgeräten repariert">
<correction glibc "Mehrere Sicherheitsprobleme behoben [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237]; Speicherzugriffsfehler auf CPUs mit AVX512-F verhindert; Use-after-free in pthread_create() behoben; in NSS-Prüfung auch auf postgresql prüfen; pthread_cond_wait() im pshared-Fall auf Nicht-x86 behoben.">
<correction gnulib "vasnprintf: Heap-Speicherüberlaufsfehler behoben [CVE-2018-17942]">
<correction gnupg2 "Absturz beim Importieren ohne TTY behoben">
<correction graphite-api "Rechtschreibung von RequiresMountsFor im systemd-Dienst behoben">
<correction grokmirror "Fehlende Abhängigkeit von python-pkg-resources behoben">
<correction gvrng "Berechtigungsproblem behoben, das den Start von gvrng verhindert hat; korrekte Python-Abhängigkeiten generieren">
<correction ibus "Mulit-Architektur-Installation durch Entfernen der gir-Abhängigkeit von Python repariert">
<correction icinga2 "Speicherung von Zeitstempeln als Lokalzeit in PostgreSQL behoben">
<correction intel-microcode "Gesammelte Korrekturen für Westmere EP eingepflegt (Signatur 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort "Python-Abhängigkeiten korrigiert">
<correction jdupes "Potenziellen Absturz auf ARM behoben">
<correction kmodpy "Inkorrektes Multi-Arch entfernt: genauso bei python-kmodpy">
<correction libapache2-mod-perl2 "Keine &lt;Perl&gt;-Abschnitte in vom Benutzer kontrollierter Konfiguration erlauben [CVE-2011-2767]">
<correction libb2 "Vor dem Verwenden von AVX auf Verfügbarkeit prüfen">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libemail-address-list-perl "Anfälligkeit für Dienstblockade behoben [CVE-2018-18898]">
<correction libemail-address-perl "Anfälligkeiten für Dienstblockade behoben [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod "python-gpod: Fehlende Abhängigkeit von python-gobject-2 hinzugefügt">
<correction libssh "Defekte serverseitige »keyboard-interactive«-Authentifizierung repariert">
<correction linux "Neue Veröffentlichung der Originalautoren; Neue Version der Originalautoren; Baufehlschläge auf arm64 und mips* behoben; libceph: CEPH_FEATURE_CEPHX_V2-Prüfung in calc_signature() korrigiert">
<correction linux-igd "Dafür gesorgt, dass das Init-Skript $network benötigt">
<correction lttng-modules "Bauprobleme auf linux-rt-4.9-Kerneln und Kerneln &gt;= 4.9.0-3 behoben">
<correction mistral "<q>std.ssh-Verhalten kann Anwesenheit von beliebigen Dateien verraten</q> behoben [CVE-2018-16849]">
<correction monkeysign "Sicherheitsproblem behoben [CVE-2018-12020]; tatsächlich mehrere Mails statt einer einzelnen versenden">
<correction mpqc "Auch sc-libtool installieren">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren">
<correction nvidia-modprobe "Neue Veröffentlichung der Originalautoren">
<correction nvidia-persistenced "Neue Veröffentlichung der Originalautoren">
<correction nvidia-settings "Neue Veröffentlichung der Originalautoren">
<correction nvidia-xconfig "Neue Veröffentlichung der Originalautoren">
<correction openni2 "armhf-Baseline-Verletzung und armel FTBFS behoben, die durch Verwendung von NEON auftreten">
<correction openvpn "NCP-Verhalten bei TLS-Neuverbindung korrigiert, das zu <q>AEAD Decrypt error: cipher final failed</q>-Fehlern geführt hat">
<correction parsedatetime "Unterstützung für Python 3 hinzugefügt">
<correction pdns "Sicherheitsprobleme behoben [CVE-2018-1046 CVE-2018-10851]; MySQL-Abfragen mit gespeicherten Prozeduren korrigiert; LDAP-, Lua- und OpenDBX-Backends repariert, welche Domains nicht gefunden haben">
<correction pdns-recursor "Sicherheitsprobleme behoben [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage "Fehlende Abhängigkeit von gir1.2-gtk-3.0 ergänzt">
<correction postfix "Neue Stable-Veröffentlichung der Originalautoren; postconf-Fehlschläge behoben, wenn postfix-instance-generator während des Bootens läuft">
<correction postgresql-9.6 "Neue Veröffentlichung der Originalautoren">
<correction postgrey "Neubau ohne Änderungen">
<correction pylint-django "Abhängigkeiten von Python 3 überarbeitet">
<correction python-acme "Rückportierung einer neueren Version wegen Missbilligung von tls-sni-01">
<correction python-arpy "Abhängigkeiten von Python 3 überarbeitet">
<correction python-certbot "Rückportierung einer neueren Version wegen Missbilligung von tls-sni-01">
<correction python-certbot-apache "Aktualisiert wegen Missbilligung von tls-sni-01">
<correction python-certbot-nginx "Aktualisiert wegen Missbilligung von tls-sni-01">
<correction python-hypothesis "(Invertierte) Abhängigkeiten von python3-hypothesis und python-hypothesis-doc umgekehrt">
<correction python-josepy "Neues Paket, benötigt von Certbot">
<correction pyzo "Fehlende Abhängigkeit von python3-pkg-resources behoben">
<correction r-cran-readxl "Absturzfehler behoben [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit "dbus und polkit von Empfehlung zur Abhängigkeit befördert">
<correction ruby-rack "Mögliche Anfälligkeit für Cross-Site Scripting behoben [CVE-2018-16471]">
<correction samba "Neue Veröffentlichung der Originalautoren; s3:ntlm_auth: Speicherleck in manage_gensec_request() behoben ;nmbd-Startfehler ignorieren, wenn es keine Nicht-Loopback-Schnittstelle oder keine lokale IPv4-Nicht-Loopback-Schnittstelle gibt; CVE-2018-14629-Rückschritt bei Nicht-CNAME-Eintrag behoben">
<correction sl-modem "Linux-Versionen &gt; 3 unterstützen">
<correction sogo-connector "Aktualisiert, um unter neueren Thunderbird-Versionen zu laufen">
<correction sox "Korrekturen für CVE-2014-8145 wirklich anwenden">
<correction ssh-agent-filter "Zwei-Byte-Stack-Schreibvorgang außerhalb der Grenzen behoben">
<correction supercollider "Unterstützung für XEmacs und Emacs &lt;=23 abgeschaltet">
<correction sympa "/etc/sympa/sympa.conf-smime.in aus den Konfigdateien entfernt; vollen Pfad für head-Befehl in der Sympa-Konfigurationsdatei benutzen">
<correction twitter-bootstrap3 "Mehrere Anfälligkeiten für Sicherheitsprobleme behoben [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata "Neue Veröffentlichung der Originalautoren">
<correction uglifyjs "Inhalte der Handbuchseite überarbeitet">
<correction uriparser "Mehrere Anfälligkeiten für Sicherheitsprobleme behoben [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm "Unterstützung für xemacs21 einstellen">
<correction vulture "Fehlende Abhängigkeit von python3-pkg-resources ergänzt">
<correction wayland "Möglichen Ganzzahl-Überlauf behoben [CVE-2017-16612]">
<correction wicd "Lieber von net-tools als Alternativen abhängen">
<correction wvstreams "Problemumgehung für Stack-Korrumpierung">
<correction xapian-core "Lecks von freelist-Blöcken in Sonderfällen, in denen sie von Database::check() als <q>DatabaseCorruptError</q> gemeldet werden, behoben">
<correction xkeycaps "Speicherzugriffsfehler in commands.c, wenn mehr als 8 keysyms pro Taste vorhanden sind, behoben">
<correction yosys "<q>ModuleNotFoundError: No module named 'smtio'</q> behoben">
<correction z3 "Inkorrektes Multi-Arch korrigiert: genauso bei python-z3">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>

<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction adblock-plus "Inkompatibel mit neueren firefox-esr-Versionen">
<correction calendar-exchange-provider "Inkompatibel mit neueren Thunderbird-Versionen">
<correction cookie-monster "Inkompatibel mit neueren firefox-esr-Versionen">
<correction corebird "Kaputt wegen Twitter-API-Änderungen">
<correction debian-buttons "Inkompatibel mit neueren firefox-esr-Versionen">
<correction debian-parl "Hängt von defekten/entfernten Firefox-Plugins ab">
<correction firefox-branding-iceweasel "Inkompatibel mit neueren firefox-esr-Versionen">
<correction firefox-kwallet5 "Inkompatibel mit neueren firefox-esr-Versionen">
<correction flashblock "Inkompatibel mit neueren firefox-esr-Versionen">
<correction flickrbackup "Inkompatibel mit derzeitiger Flickr-API">
<correction imap-acl-extension "Inkompatibel mit neueren firefox-esr-Versionen">
<correction libwww-topica-perl "Nutzlos, seit die Topica-Seite abgeschaltet wurde">
<correction mozilla-dom-inspector "Inkompatibel mit neueren firefox-esr-Versionen">
<correction mozilla-noscript "Inkompatibel mit neueren firefox-esr-Versionen">
<correction mozilla-password-editor "Inkompatibel mit neueren firefox-esr-Versionen">
<correction mozvoikko "Inkompatibel mit neueren firefox-esr-Versionen">
<correction personaplus "Inkompatibel mit neueren firefox-esr-Versionen">
<correction python-formalchemy "Unbrauchbar, lässt sich nicht in Python importieren">
<correction refcontrol "Inkompatibel mit neueren firefox-esr-Versionen">
<correction requestpolicy "Inkompatibel mit neueren firefox-esr-Versionen">
<correction spice-xpi "Inkompatibel mit neueren firefox-esr-Versionen">
<correction toggle-proxy "Inkompatibel mit neueren firefox-esr-Versionen">
<correction y-u-no-validate "Inkompatibel mit neueren firefox-esr-Versionen">

</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://security.debian.org/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Kraft und Zeit einbringen, um das vollständig freie Betriebssystem 
Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt;, oder kontaktieren das Stable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>
